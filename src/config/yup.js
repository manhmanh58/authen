import * as Yup from "yup";

const defaultMessage = {
  mixed: {
    required: "${label} must be required",
  },
  string: {
    min: "${label} must be greater than ${min} charater",
    max: "${label} must be less than ${max} characters",
    email: "Email is invalid",
  },
};
function isValidPassword() {
  return this.required("Please Enter your password")
    .min(8)
    .max(21)
    .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#%&])(?=.{8,})/, {
      message:
        "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character",
    });
}
function isValidAge() {
  return this.matches(/^\d+$/, {
    message: "The field should have digits only",
  });
}
function isValidPhone() {
  return this.required().matches(/((09|03|07|08|05)+([0-9]{8})\b)/g, {
    message: "Invalid phone number",
  });
}

Yup.addMethod(Yup.string, "isValidPhone", isValidPhone);
Yup.addMethod(Yup.string, "isValidPassword", isValidPassword);
Yup.addMethod(Yup.string, "isValidAge", isValidAge);
Yup.setLocale(defaultMessage);

export default Yup;
