import Yup from "../config/yup";

export const schema = Yup.object().shape({
  firstName: Yup.string()
    .label("First name")
    .required()
    .min(2, "First name must be greater than 2 characters"),

  lastName: Yup.string()
    .label("Last name")
    .required()
    .min(2, "Last name must be greater than 10 characters"),

  age: Yup.string().label("Age").required().isValidAge(),

  confirmPwd: Yup.string()
    .label("Comfirm Password")
    .required("Please confirm password")
    .oneOf([Yup.ref("password")], "Passwords does not match"),
  phoneNumber: Yup.string().label("Phone number").isValidPhone(),
});
