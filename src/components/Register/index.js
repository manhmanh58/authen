import styles from "./style.module.scss";
import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schema } from "../../validation/validation";
import { useNavigate } from "react-router-dom";
import { REGISTER_URL } from "../../constants/common";
import useToken from "../../hooks/useToken";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Register() {
  const [formData, setFormData] = useState({
    password: "",
    email: "",
  });
  const notify = (text) => toast(text);

  const {
    register,
    handleSubmit,

    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const navigate = useNavigate();
  const { token } = useToken();
  const onSubmit = () => {
    fetch(`${REGISTER_URL}`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    })
      .then((res) => {
        if (res.ok) {
          navigate("/login");
          return res.json();
        }
      })
      .catch(notify("Email already exists"));
  };

  function handleChange(e) {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  }
  useEffect(() => {
    if (token) {
      navigate("/");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className={styles.formContainer}>
      <h1 style={{ textAlign: "center", color: "#fff" }}>Register</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        {/* Full name */}
        <div>
          <label>Full Name</label>
          <input
            {...register("firstName", {
              required: "This is required",
            })}
            defaultValue=""
            placeholder="FirtName"
          />
          <p>{errors.firstName?.message}</p>
        </div>
        <div>
          <input
            {...register("lastName", {})}
            defaultValue=""
            placeholder="LastName"
          />
          <p>{errors.lastName?.message}</p>
        </div>

        {/* Email */}
        <div>
          <label>Email</label>
          <input
            {...register("email")}
            placeholder="email"
            type="email"
            onChange={(e) => handleChange(e)}
          />
          <p>{errors.email?.message}</p>
        </div>

        {/* Password */}
        <div>
          <label>Password</label>
          <input
            name="password"
            type="password"
            {...register("password")}
            onChange={(e) => handleChange(e)}
          />
          <p>{errors.password?.message}</p>
        </div>
        <div>
          <label>Confirm Password</label>
          <input
            name="confirmPwd"
            type="password"
            {...register("confirmPwd")}
          />
          <p>{errors.confirmPwd?.message}</p>
        </div>
        {/* Phone Number */}
        <div>
          <label>Phone Number</label>
          <input defaultValue="" {...register("phoneNumber", {})} />
          <p>{errors.phoneNumber?.message}</p>
        </div>
        {/* Gender */}
        <div>
          <label>Gender</label>
          <select {...register("gender")}>
            <option value="female">Female</option>
            <option value="male">Male</option>
            <option value="other">Other</option>
          </select>
        </div>
        {/* Age */}
        <label>Age</label>
        <input {...register("age", {})} />
        <p>{errors.age?.message}</p>
        <button type="Submit">Register</button>
      </form>
      <ToastContainer limit={3} />
    </div>
  );
}

export default Register;
