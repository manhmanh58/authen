import React from "react";

import styles from "./style.module.scss";
import useToken from "../../hooks/useToken";
import CommonHeader from "./common";
const LoggedHeader = () => {
  const { remove } = useToken();
  return (
    <div className={styles.header}>
      <div className={styles[`header__function`]}>
        <CommonHeader />
      </div>
      <div>
        <a href="/login" className={styles.login} onClick={remove}>
          <div className={styles.button}>Logout</div>
        </a>
      </div>
    </div>
  );
};

export default LoggedHeader;
