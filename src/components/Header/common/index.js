import React from "react";
import { NavLink } from "react-router-dom";
import styles from "./style.module.scss";
const CommonHeader = () => {
  return (
    <>
      <ul className={styles[`header__list`]}>
        <li>
          <NavLink to="/">Home</NavLink>
        </li>
        <li>
          <NavLink to="/Dashboard">Dashboard</NavLink>
        </li>
        <li>
          <NavLink to="/Preferences">Preferences</NavLink>
        </li>
      </ul>
    </>
  );
};
export default CommonHeader;
