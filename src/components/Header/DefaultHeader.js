import React from "react";
import { NavLink } from "react-router-dom";
import styles from "./style.module.scss";
import CommonHeader from "./common";
const DefaultHeader = () => {
  return (
    <div className={styles.header}>
      <div className={styles[`header__function`]}>
        <CommonHeader />
      </div>
      <div>
        <NavLink className={styles.login} to="/Login">
          <div className={styles.button}>Login</div>
        </NavLink>
        <NavLink className={styles.register} to="/Register">
          <div className={styles.button}>Register</div>
        </NavLink>
      </div>
    </div>
  );
};

export default DefaultHeader;
