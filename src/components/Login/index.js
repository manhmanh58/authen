import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import styles from "./style.module.scss";
import { useForm } from "react-hook-form";
import { NavLink } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import { LOGIN_URL } from "./../../constants/common";
import { ToastContainer, toast } from "react-toastify";
import useToken from "../../hooks/useToken";

export default function Login({ setToken }) {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });
  const { token } = useToken();
  const notify = (text) => toast(text);
  const { register, handleSubmit } = useForm();
  const navigate = useNavigate();

  const onSubmit = (e) => {
    fetch(`${LOGIN_URL}`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    })
      .then((res) => {
        if (res.ok) {
          navigate("/");
          return res.json();
        }
      })
      .then((data) => setToken(data.accessToken))
      .catch(notify("Incorrect UserName or Password"));
  };

  function handleChange(e) {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  }

  useEffect(() => {
    if (token) {
      navigate("/");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className={styles.formContainer}>
      <h1 style={{ textAlign: "center", color: "#fff" }}>Login</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div>
          <label>Email</label>
          <input
            {...register("email")}
            placeholder="email"
            type="email"
            onChange={(e) => handleChange(e)}
          />
        </div>

        <div>
          <label>Password</label>
          <input
            name="password"
            type="password"
            {...register("password")}
            onChange={(e) => handleChange(e)}
          />
        </div>
        <button type="Submit">Login</button>

        <p className={styles[`check-box`]}>
          Do not have an account? <NavLink to="/Register">Register</NavLink>
        </p>
      </form>
      <ToastContainer limit={3} />
    </div>
  );
}
