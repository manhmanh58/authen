import React from "react";
import { Outlet } from "react-router-dom";
import Layout from "../layouts";

const DefaultRoute = ({ layout }) => {
  const LayoutTemplate = Layout(layout);
  return (
    <LayoutTemplate>
      <Outlet />
    </LayoutTemplate>
  );
};
export default DefaultRoute;
