import Dashboard from "../components/Dashboard";
import Home from "../components/Home/Home";
import Login from "../components/Login";
import Preferences from "../components/Preferences";
import { Routes, Route } from "react-router-dom";
import Register from "../components/Register";
import DefaultRoute from "./DefaultRoute";
import PrivateRoute from "./PrivateRoute";
import useToken from "../hooks/useToken";

export default function Routers() {
  const { token, setToken } = useToken();
  return (
    <Routes>
      <Route
        exact
        path="/"
        element={<DefaultRoute layout={token ? "logged" : "default"} />}
      >
        <Route exact path="/" element={<Home />} />
        <Route exact path="/Login" element={<Login setToken={setToken} />} />
        <Route exact path="/Register" element={<Register />} />
      </Route>
      <Route exact path="/" element={<PrivateRoute layout="logged" />}>
        <Route exact path="/Dashboard" element={<Dashboard />} />
        <Route exact path="/Preferences" element={<Preferences />} />
      </Route>
    </Routes>
  );
}
