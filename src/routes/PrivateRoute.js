import { Navigate, Outlet } from "react-router-dom";
import useToken from "../hooks/useToken";

import Layout from "../layouts";

const PrivateRoute = ({ layout }) => {
  const { token } = useToken();
  const LayoutTemplate = Layout(layout);
  console.log(token);
  if (token) {
    return (
      <LayoutTemplate>
        <Outlet />
      </LayoutTemplate>
    );
  }
  return (
    <>
      <Navigate to="/login" replace={true} />
    </>
  );
};
export default PrivateRoute;
