import React from "react";
import { LoggedHeader } from "./../../components/Header";

const LoggedLayout = ({ children }) => {
  return (
    <>
      <LoggedHeader />
      {children}
    </>
  );
};
export default LoggedLayout;
