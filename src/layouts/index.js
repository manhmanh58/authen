import DefaultLayout from "./DefaultLayout";
import LoggedLayout from "./LoggedLayout";

const Layout = (type) => {
  switch (type) {
    case "default":
      return DefaultLayout;
    case "logged":
      return LoggedLayout;
    default:
      return DefaultLayout;
  }
};

export default Layout;
