import React from "react";
import { DefaultHeader } from "./../../components/Header";

const DefaultLayout = ({ children }) => {
  return (
    <>
      <DefaultHeader />
      {children}
    </>
  );
};
export default DefaultLayout;
